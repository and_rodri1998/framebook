
import React, { Component} from 'react';
import {
  StyleSheet
} from 'react-native';
import { Router, Scene,Stack} from 'react-native-router-flux';

import {styles} from '../style';

import TelaSwitch from '../telas/TelaSwitch';
import Login from '../telas/Login';
import Cadastro from '../telas/Cadastro';
import CompCadastro from '../telas/CompCadastro';
import Inicio from '../telas/Inicio';
import CadastroLivro from '../telas/CadastroLivro';
import Perfil from '../telas/Perfil';
import SolicitacaoLivro from '../telas/SolicitacaoLivro';
import MeusEmprestimos from '../telas/MeusEmprestimos';
import MinhasSolicitacoes from '../telas/MinhasSolicitacoes';
import TelaDevolver from '../telas/TelaDevolver';
import TelaEmprestar from '../telas/TelaEmprestar';
import MeusLivros from '../telas/MeusLivros';
import SplashScreen from '../telas/SplashScreen';

import TabIcon from './TabIcon';

export default class RouterComponent extends Component{
  render(){
    return (
      <Router style={{paddingTop:65}}>
        <Stack key="root">
          <Scene key="SplashScreen" type="reset" component={SplashScreen} title="Aguarde" hideNavBar={true} initial/>
          <Scene key="TelaSwitch" type="reset" component={TelaSwitch} title="Você escolhe" hideNavBar={true} />
          <Scene key="Login" component={Login} title="Login" hideNavBar={true}/>
          <Scene key="Cadastro" component={Cadastro} title="Cadastro" hideNavBar={true}/>
          <Scene key="CompCadastro" component={CompCadastro} title="Complemento de Cadastro" hideNavBar={true}/>
          
          <Scene
              key="appShow"
              tabs={true} 
              hideNavBar 
              showLabel={false}
              activeTintColor='#fafafa'
              inactiveTintColor='#CACACA'
              tabBarStyle={styles.tabBarStyle} >
              <Stack key="Perfil" title="Perfil" hideNavBar={true} icon={TabIcon}>
                <Scene key="PerfilInicio" component={Perfil} title="Perfil" hideNavBar={true}/>
                <Scene key="CadastrarLivro" component={CadastroLivro} title="Adicionar Livro" hideNavBar={true} />
              </Stack>
              <Stack key="Inicio" title="Inicio" hideNavBar={true} icon={TabIcon} initial>
                <Scene key="InicioScreen" component={Inicio} title="Home" hideNavBar={true} icon={TabIcon}/>
                <Scene key="MeusLivros" component={MeusLivros} title="Meus Livro" hideNavBar={true} />
              </Stack>
          </Scene>
          <Scene key="SolicitarLivro" component={SolicitacaoLivro} title="Solicitar Livro" hideNavBar={true}/>
          <Scene key="MeusEmprestimos" component={MeusEmprestimos} title="Meus Empréstimos" hideNavBar={true}/>
          <Scene key="MinhasSolicitacoes" component={MinhasSolicitacoes} title="Minhas Solicitações" hideNavBar={true}/>
          <Scene key="TelaDevolver" component={TelaDevolver} title="Devolver" hideNavBar={true}/>
          <Scene key="TelaEmprestar" component={TelaEmprestar} title="Emprestar" hideNavBar={true}/>
        </Stack>
      </Router>
    )
  }
}

const style=StyleSheet.create({
  tabBarStyle:{
    height:60,
    backgroundColor:'#38006b',
  },
  lblColor:{
    color:'#fff'
  }
})
