import React,{Component} from 'react';
import {
  View,
  Text
} from 'react-native';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default class TabIcon extends Component {
  retIcons(title){
    if (title=="Perfil") {
      return "person";
    }else if(title="Home"){
      return "home";
    }
  }

  render() {
    /** some styling **/
    let icon=this.retIcons(this.props.title);
    var color = this.props.focused
            ? this.props.activeTintColor 
            : this.props.inactiveTintColor
    return (
        <View style={{alignItems:'center',justifyContent:'center'}}>
          <MaterialIcons name={icon} size={30} color={color} />
          <Text style={{color: color,fontSize:12}}>{this.props.title}</Text>
        </View>
    );
  }
}
