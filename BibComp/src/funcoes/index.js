import React from 'react';
import {Actions} from 'react-native-router-flux';
import firebase from 'react-native-firebase';
import md5 from "react-native-md5";

export const goLogin=()=>{
    Actions.push('Login');
}
export const goCadastro=()=>{
    Actions.push('Cadastro');
}
export const goInicio=()=>{
    Actions.reset('appShow');
}
export const goCompCadastro=(email,senha,username)=>{
    Actions.push('CompCadastro',{email,senha,username});
}
export const goTelaEmprestar=()=>{
    Actions.push('TelaEmprestar');
}
export const goMeusLivros=()=>{
    Actions.push('MeusLivros');
}
export const goCadLivro=()=>{
    Actions.push('CadastrarLivro');
}
export const goDevolver=()=>{
    Actions.push('TelaDevolver');
}
export const goSolLivro=()=>{
    Actions.push('SolicitarLivro');
}
export const goMeusEmprestimos=()=>{
    Actions.push('MeusEmprestimos');
}
export const goMinhsSoliticoes=()=>{
    Actions.push('MinhasSolicitacoes');
}
export const goSwitch=()=>{
    Actions.push('TelaSwitch');
}
export const popSwitch=()=>{
    Actions.popTo('TelaSwitch');
}
export const popToCadastro=()=>{
    Actions.popTo('Cadastro');
}
export const popToPerfil=()=>{
    Actions.popTo('PerfilInicio');
}
export const popToInicio=()=>{
    Actions.popTo('InicioScreen');
}
export const popToMeusEmpr=()=>{
    Actions.popTo('MeusEmprestimos');
}

// login

export const logar=(email,password)=>{
    // createUserWithEmailAndPassword(email, password)
    firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
        alert('Não foi possível realizar o login, email ou senhas incorretos!');
    });
}

export const logout=()=>{
    firebase.auth().signOut().then(function() {
      alert('Bye Bye, Áté mais bebê e volte logo');
    }).catch(function(error) {
      alert('Erro ao realizar logout');
    });
}

// Cadastro

export const cadastroUser=(email, password)=>{
    firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log(errorMessage)
      alert("Erro ao cadastrar usuário");
    });
}

export const compCadUser=async(image,dataUser,endereco,telefone,
    interesses,username)=> {

    let img=md5.hex_md5( Date.now() +"" ),urlDownRet:'';
    const imageRef = firebase.storage().ref('perfis').child(img);

    urlDownRet = await imageRef.putFile(image.path, {contentType: image.type}).then(function(){
        return imageRef.getDownloadURL();
    }).then((url)=>{
        console.log("Image url", {url:url});
        return url;
    }).catch((error)=>{
        console.log("Error while saving the image.. ", error);
    });

    let userFull={
        username,
        imgperfil:urlDownRet,
        endereco,
        telefone,
        interesses
    }

    firebase.database().ref('users/'+dataUser._user.uid).set(userFull).then(()=>{
        alert("Cadastro realizado com sucesso");
    }).catch((error)=>{
        alert("erro ao realizar cadastro");
        console.log(error);
    })

}