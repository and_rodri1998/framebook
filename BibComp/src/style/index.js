import React,{Component} from 'react';
import {
    StyleSheet
} from 'react-native';

import {primaryLightColor,whiteBack} from '../config/constantes';

export const styles= StyleSheet.create({
  // styles gerais
    container: {
        flex: 1,
        backgroundColor: primaryLightColor,
    },
    areaConteudo:{
        flex:1,
    },
    areaConteudoSwitch:{
      flex:1,
      alignItems:'center',
      justifyContent:'space-between'
    },
    areaCenter:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        paddingBottom:10
    },
    slogan:{
      fontFamily:'JosefinSans-Regular',
      fontSize:20,
      color:'white'
    },
    nameLog:{
      fontFamily:'JosefinSans-Regular',
      fontSize:50,
      color:'white'
    },
    row:{
      
    },
    tabBarStyle:{
        height:60,
        backgroundColor:primaryLightColor
    },
    header:{
      height:60,
      backgroundColor:primaryLightColor,
      paddingHorizontal:10,
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'space-between'
    },
    // tela switch
    buttonSwitch:{
      height:50,
      width:250,
      alignItems:'center',
      justifyContent:'center',
      marginBottom:10,
      backgroundColor:'white',
      borderRadius:50
    },
    textBtnSwitch:{
      fontSize:20,
      fontFamily:'JosefinSans-Bold',
      color:primaryLightColor
    },
    // tela login
    inputLog:{
      height: 40,
      width:'90%',
      backgroundColor:'rgba(255,255,255,0.5)',
      borderRadius:10,
      paddingHorizontal:10,
      marginBottom:10,
      color:'white'
    },
    btnLogCad:{
      width:'49%',
      alignItems:'center',
      justifyContent:'center',
      borderRadius:10,
      backgroundColor:'white',
      height:40
    },
    textBtnLog:{
      fontSize:16,
      fontFamily:'JosefinSans-Bold',
      color:primaryLightColor
    },
    // complemento de cadastro
    ordChecks:{
      width:'90%', 
      marginBottom:10,
      marginTop:10
    },
    alingChecks:{
      flexDirection:'row',
      justifyContent:'space-between'
    },
    checkStyle:{
      width:'44%',
      maxWidth:'44%',
      padding:0,
      backgroundColor:'transparent',
      borderWidth:0
    },
    btnLogAltFt:{
      alignItems:'center',
      justifyContent:'center',
      borderRadius:10,
      width:100,
      height:30,
      backgroundColor:'white',
      marginTop:10
    },
    // tela inicial
    containerIconButton:{
      alignItems:'center',
      justifyContent:'center'
    },
    containerButtons:{
      width:230,
      flexDirection:'row',
      marginTop:20,
      alignItems:'center',
      justifyContent:'space-between'
    }
});
