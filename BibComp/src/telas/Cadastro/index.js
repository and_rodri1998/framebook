import React,{Component} from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    TextInput,
    StatusBar
} from 'react-native';

import {styles} from '../../style';
import {popSwitch,goCompCadastro} from '../../funcoes';
import {primaryLightColor} from '../../config/constantes';

export default class Cadastro extends Component{
    constructor(props){
      super(props);
      this.state={
        email:'',
        senha:'',
        confSenha:'',
        username:''
      }
    }
    verificarCampos(){
        if(this.state.email==''||this.state.senha==''||
          this.state.confSenha==''||this.state.username==''){
            alert(`existem campos vazios`);
          }
        else{
          if(this.state.senha==this.state.confSenha)
            goCompCadastro(this.state.email,this.state.senha,this.state.username);
          else
            alert('Senhas não conferem')
        }
    }
    render(){
        return (
          <View style={styles.container}>
              <StatusBar
                    backgroundColor={primaryLightColor}
                    barStyle="light-content"
                />
              <ScrollView contentContainerStyle={{flexGrow: 1}}>
                <View style={styles.areaCenter}>
                  <Text style={styles.nameLog}>Frame Book</Text>
                  <TextInput
                      autoCorrect={false}
                      autoCapitalize="none"
                      returnKeyType="next"
                      style={styles.inputLog}
                      onChangeText={(username) => {this.setState({username})}}
                      placeholder="Nome de Usuário"
                      placeholderTextColor="#fff"
                  />
                  <TextInput
                      autoCorrect={false}
                      returnKeyType="next"
                      autoCapitalize="none"
                      style={styles.inputLog}
                      onChangeText={(email) => {this.setState({email})}}
                      placeholder="Email"
                      keyboardType="email-address"
                      placeholderTextColor="#fff"
                  />
                  <TextInput
                      autoCorrect={false}
                      returnKeyType="next"
                      autoCapitalize="none"
                      style={styles.inputLog}
                      onChangeText={(senha) => {this.setState({senha})}}
                      placeholder="Senha"
                      placeholderTextColor="#fff"
                      secureTextEntry={true}
                  />
                  <TextInput
                      autoCorrect={false}
                      returnKeyType="send"
                      autoCapitalize="none"
                      style={styles.inputLog}
                      onChangeText={(confSenha) => {this.setState({confSenha})}}
                      placeholder="Confirme a senha"
                      placeholderTextColor="#fff"
                      secureTextEntry={true}
                  />
                  <View style={{width:'90%',flexDirection:'row',justifyContent:'space-between'}}>
                        <TouchableOpacity style={styles.btnLogCad} onPress={()=>popSwitch()}>
                            <Text style={styles.textBtnLog}>Voltar</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btnLogCad} onPress={()=>this.verificarCampos()}>
                            <Text style={styles.textBtnLog}>Continuar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
              </ScrollView>
          </View>
        )
    }
}
