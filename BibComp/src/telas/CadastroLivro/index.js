import React,{Component} from 'react';
import {
    View,
    Text,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Image,
    StatusBar
} from 'react-native';

import {
    CheckBox
} from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';

import {styles} from '../../style';
import {goInicio,popToPerfil} from '../../funcoes';
import {
    primaryTextColor,
    secondaryDarkColor,
    whiteBack,
    primaryLightColor,
    secondaryColor
} from '../../config/constantes';
import HeaderComum from '../componentes/Header/HeaderComum';

export default class CadastroLivro extends Component{
    constructor(props){
        super(props);
        this.state={avatarSource:require('../../assets/img/baseline_book_black_48.png'),literatura:false,tecnologia:false,esportes:false,outros:false};
    }

    getImagem(){
        let options = {
          title: 'Selecionar capa do livro',
          // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };

        ImagePicker.launchImageLibrary(options, (response) => {
          console.log('Response = ', response);

          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            let source = { uri: response.uri };

            this.setState({
              avatarSource: source
            });
          }
        });
    }

    render(){

        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={primaryLightColor}
                    barStyle="light-content"
                />
                <HeaderComum title="Adicionar Livro"/>
                <ScrollView contentContainerStyle={{flexGrow: 1}}>
                  <View style={styles.areaCenter}>
                    <View style={{marginTop:10,marginBottom:10, alignItems:'center'}}>
                        <View style={{padding:5,width: 155, height: 155,borderRadius:150,backgroundColor:'white',alignItems:'center', justifyContent:'center'}}>
                            <Image
                              style={{width: 150, height: 150,borderRadius:150}}
                              source={this.state.avatarSource}
                            />
                        </View>
                        <TouchableOpacity style={styles.btnLogAltFt} onPress={()=>{this.getImagem();}}>
                            <Text style={styles.textBtnLog}>Alterar</Text>
                        </TouchableOpacity>
                        <Text style={styles.slogan}>Capa do Livro</Text>
                    </View>
                    <TextInput
                        style={styles.inputLog}
                        onChangeText={(text) => {}}
                        placeholder="Título"
                        placeholderTextColor="#fff"
                    />
                    <TextInput
                        style={styles.inputLog}
                        onChangeText={(text) => {}}
                        placeholder="Edição"
                        placeholderTextColor="#fff"
                    />
                    <TextInput
                        style={styles.inputLog}
                        onChangeText={(text) => {}}
                        placeholder="Autor"
                        placeholderTextColor="#fff"
                    />
                    <Text style={styles.slogan}>Tema do Livro</Text>
                    <View style={styles.ordChecks}>
                        <View style={styles.alingChecks}>
                            <CheckBox
                                containerStyle={styles.checkStyle}
                                textStyle={{color:"white"}}
                                fontFamily='JosefinSans-Regular'
                                title='Literatura'
                                checked={this.state.literatura}
                                checkedColor="white"
                                onPress={()=>{
                                    let check=false;
                                    check=!this.state.literatura;
                                    this.setState({literatura:check});
                                }}
                            />
                            <CheckBox
                                containerStyle={styles.checkStyle}
                                textStyle={{color:"white"}}
                                fontFamily='JosefinSans-Regular'
                                title='Tecnologia'
                                checked={this.state.tecnologia}
                                checkedColor="white"
                                onPress={()=>{
                                    let check=false;
                                    check=!this.state.tecnologia;
                                    this.setState({tecnologia:check});
                                }}
                            />
                        </View>
                        <View style={styles.alingChecks}>
                            <CheckBox
                                containerStyle={styles.checkStyle}
                                textStyle={{color:"white"}}
                                fontFamily='JosefinSans-Regular'
                                title='Esportes'
                                checked={this.state.esportes}
                                checkedColor="white"
                                onPress={()=>{
                                    let check=false;
                                    check=!this.state.esportes;
                                    this.setState({esportes:check});
                                }}
                            />
                            <CheckBox
                                containerStyle={styles.checkStyle}
                                textStyle={{color:"white"}}
                                fontFamily='JosefinSans-Regular'
                                title='Outros'
                                checked={this.state.outros}
                                checkedColor="white"
                                onPress={()=>{
                                    let check=false;
                                    check=!this.state.outros;
                                    this.setState({outros:check});
                                }}
                            />
                        </View>
                    </View>
                    <View style={{width:'90%',flexDirection:'row',justifyContent:'space-between'}}>
                        <TouchableOpacity style={[styles.btnLogCad,{backgroundColor:secondaryColor}]} onPress={()=>popToPerfil()}>
                            <Text style={[styles.textBtnLog,{color:'white'}]}>Cancelar</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btnLogCad} onPress={()=>popToPerfil()}>
                            <Text style={styles.textBtnLog}>Salvar</Text>
                        </TouchableOpacity>
                    </View>
                  </View>
                </ScrollView>
            </View>
        )
    }
}
