import React,{Component} from 'react';
import {
    View,
    Text,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Image,
    StatusBar,
    Platform
} from 'react-native';

import {CheckBox} from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
import firebase from 'react-native-firebase';

import {styles} from '../../style';
import {goInicio,cadastroUser,compCadUser} from '../../funcoes';
import {primaryTextColor,primaryLightColor} from '../../config/constantes';
export default class CompCadastro extends Component{

    constructor(props){
        super(props);
        this.state={
            avatarSource:require('../../assets/img/baseline_person_black_48.png'),
            imagem:null,
            literatura:false,tecnologia:false,esportes:false,outros:false,
            endereco:'',
            telefone:''
        };
    }

    componentDidMount(){
        firebase.auth().onAuthStateChanged((user) =>{
          if (user) {
            compCadUser(this.state.imagem,user,this.state.endereco,this.state.telefone,
                {literatura:this.state.literatura,tecnologia:this.state.tecnologia,
                    esportes:this.state.esportes,outros:this.state.outros},this.props.username);
          }
        });
    }

    verificarCampos(){
        if(this.props.email==''||this.props.senha==''||this.props.username==''
            ||this.state.endereco==''||this.state.telefone=='')
            alert('existem campos vazios');
        else{
            cadastroUser(this.props.email,this.props.senha);
        }
    }

    getImagem(){
        let options = {
          title: 'Selecionar foto de perfil',
          // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };

        ImagePicker.launchImageLibrary(options, (response) => {
          console.log('Response = ', response);

          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            let source = { uri: response.uri };

            this.setState({
              avatarSource: source,imagem:response
            });
          }
        });
    }

    render(){

        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={primaryLightColor}
                    barStyle="light-content"
                />
                <ScrollView contentContainerStyle={{flexGrow: 1}}>
                  <View style={styles.areaCenter}>
                    <Text style={styles.nameLog}>Editar perfil</Text>
                    <View style={{marginTop:10,marginBottom:10, alignItems:'center'}}>
                        <View style={{padding:5,width: 155, height: 155,borderRadius:150,backgroundColor:'white',alignItems:'center', justifyContent:'center'}}>
                            <Image
                              style={{width: 150, height: 150,borderRadius:150}}
                              source={this.state.avatarSource}
                            />
                        </View>
                        <TouchableOpacity style={styles.btnLogAltFt} onPress={()=>{this.getImagem();}}>
                            <Text style={styles.textBtnLog}>Alterar</Text>
                        </TouchableOpacity>
                        <Text style={styles.slogan}>Editar foto de perfil</Text>
                    </View>
                    <TextInput
                        style={styles.inputLog}
                        onChangeText={(endereco) => {this.setState({endereco})}}
                        placeholder="Endereço"
                        placeholderTextColor="#fff"
                    />
                    <TextInput
                        style={styles.inputLog}
                        onChangeText={(telefone) => {this.setState({telefone})}}
                        placeholder="Telefone"
                        keyboardType="phone-pad"
                        placeholderTextColor="#fff"
                    />
                    <Text style={styles.slogan}>Áreas de Interesse</Text>
                    <View style={styles.ordChecks}>
                        <View style={styles.alingChecks}>
                            <CheckBox
                                containerStyle={styles.checkStyle}
                                textStyle={{color:'white'}}
                                fontFamily='JosefinSans-Regular'
                                title='Literatura'
                                checked={this.state.literatura}
                                checkedColor={primaryTextColor}
                                onPress={()=>{
                                    let check=false;
                                    check=!this.state.literatura;
                                    this.setState({literatura:check});
                                }}
                            />
                            <CheckBox
                                containerStyle={styles.checkStyle}
                                textStyle={{color:'white'}}
                                fontFamily='JosefinSans-Regular'
                                title='Tecnologia'
                                checked={this.state.tecnologia}
                                checkedColor={primaryTextColor}
                                onPress={()=>{
                                    let check=false;
                                    check=!this.state.tecnologia;
                                    this.setState({tecnologia:check});
                                }}
                            />
                        </View>
                        <View style={styles.alingChecks}>
                            <CheckBox
                                containerStyle={styles.checkStyle}
                                textStyle={{color:'white'}}
                                fontFamily='JosefinSans-Regular'
                                title='Esportes'
                                checked={this.state.esportes}
                                checkedColor={primaryTextColor}
                                onPress={()=>{
                                    let check=false;
                                    check=!this.state.esportes;
                                    this.setState({esportes:check});
                                }}
                            />
                            <CheckBox
                                containerStyle={styles.checkStyle}
                                textStyle={{color:'white'}}
                                fontFamily='JosefinSans-Regular'
                                title='Outros'
                                checked={this.state.outros}
                                checkedColor={primaryTextColor}
                                onPress={()=>{
                                    let check=false;
                                    check=!this.state.outros;
                                    this.setState({outros:check});
                                }}
                            />
                        </View>
                    </View>
                    <View style={{width:'90%',flexDirection:'row',justifyContent:'space-between'}}>
                        <TouchableOpacity style={styles.btnLogCad} onPress={()=>goInicio()}>
                            <Text style={styles.textBtnLog}>Início</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btnLogCad} onPress={()=>this.verificarCampos()}>
                            <Text style={styles.textBtnLog}>Salvar</Text>
                        </TouchableOpacity>
                    </View>
                  </View>
                </ScrollView>
            </View>
        )
    }
}
