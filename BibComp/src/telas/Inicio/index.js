import React,{Component} from 'react';
import {
    View,
    Text,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Image,
    StatusBar,
    ActivityIndicator
} from 'react-native';

import {CheckBox,SearchBar} from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
import Ripple from 'react-native-material-ripple';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ElevatedView from 'react-native-elevated-view';
import firebase from 'react-native-firebase';

import {styles} from '../../style';
import {goCadLivro,goMeusEmprestimos,goMeusLivros,goMinhsSoliticoes} from '../../funcoes';
import {primaryTextColor,primaryLightColor} from '../../config/constantes';
import HeaderComum from '../componentes/Header/HeaderComum';
import HeaderInicio from '../componentes/Header/HeaderInicio';
import ListaInteresse from '../Listas/ListaInteresse';
import ListaLivrosInicio from '../Listas/ListaLivrosInicio';

export default class Inicio extends Component{
    constructor(props){
        super(props);
        this.state={
            user:''
            ,avatarSource:require('../../assets/img/baseline_person_black_48.png'),literatura:false,tecnologia:false,esportes:false,outros:false};
    }

    componentDidMount(){
        this.getPerfil();
    }

    getPerfil(){
        let uid=firebase.auth().currentUser.uid,user=null;
        firebase.database().ref('/users/'+uid).once('value').then((snapshot)=>{
            user=snapshot.val();
            // alert(user.username);
            
            this.setState({user});
        })
    }

    render(){
        let view=null;
        if(!this.state.user){

            view = <View style={styles.container}>
                        <StatusBar
                            backgroundColor={primaryLightColor}
                            barStyle="light-content"
                        />
                      <View style={styles.areaCenter}>
                        <ActivityIndicator size="large" />
                        <Text style={[styles.nameLog,{fontSize:16}]}>carregando</Text>
                        <Text style={[styles.nameLog,{fontSize:16}]}>dados</Text>
                      </View>
                    </View>
        }else{
            view = (
                <View style={styles.container}>
                    <StatusBar
                        backgroundColor={primaryLightColor}
                        barStyle="light-content"
                    />
                    <HeaderInicio title="Início"/>
                    <ScrollView contentContainerStyle={{flexGrow: 1}}>
                      <View style={[styles.areaConteudo,{paddingBottom:10}]}>
                        <View style={{marginTop:10,marginBottom:10, alignItems:'center'}}>
                            <View style={{padding:5,width: 105, height: 105,borderRadius:105,backgroundColor:'white',alignItems:'center', justifyContent:'center'}}>
                                <Image
                                  style={{width: 100, height: 100,borderRadius:100}}
                                  source={{uri:this.state.user.imgperfil}}
                                />
                            </View>
                            <Text style={styles.slogan}>{this.state.user.username}</Text>
                            <View style={styles.containerButtons}>
                                <View style={styles.containerIconButton}>
                                    <Ripple onPress={()=>goMeusLivros()}>
                                        <MaterialIcons name='view-week' size={40} color='#fff' />
                                    </Ripple>
                                    <Text style={{color: '#fff',fontSize:12}}>Meus Livros</Text>
                                </View>
                                <View style={styles.containerIconButton}>
                                    <Ripple onPress={()=>goMeusEmprestimos()}>
                                        <MaterialIcons name='account-balance-wallet' size={40} color='#fff' />
                                    </Ripple>
                                    <Text style={{color: '#fff',fontSize:12}}>Meus Empr.</Text>
                                </View>
                                <View style={styles.containerIconButton}>
                                    <Ripple onPress={()=>goMinhsSoliticoes()}>
                                        <MaterialIcons name='library-add' size={40} color='#fff' />
                                    </Ripple>
                                    <Text style={{color: '#fff',fontSize:12}}>Solicitações</Text>
                                </View>
                            </View>
                            <View style={{paddingHorizontal:10,width:'100%',marginTop:10}}>
                                <ElevatedView elevatio={2} style={{padding:10,borderRadius:10}}>
                                    <Text style={[styles.slogan,{color:primaryLightColor,alignSelf:'center',marginBottom:10}]}>Livros Disponíveis</Text>
                                    <TextInput
                                        style={[styles.inputLog,{marginBottom:10,backgroundColor:'rgba(0,0,0,0.4)',width:'100%'}]}
                                        onChangeText={(text) => {}}
                                        placeholder="Buscar"
                                        placeholderTextColor={primaryLightColor}
                                    />
                                    <ListaLivrosInicio />
                                </ElevatedView>
                            </View>
                        </View>
                      </View>
                    </ScrollView>
                </View>
            )
        }

        return view;
    }
}
