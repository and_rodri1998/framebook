import React,{Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image
} from 'react-native';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import {styles} from '../../../style';

export default class ItemListaInteresse extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <View style={{padding:5,alignItens:'center',justifyContent:'center'}}><Text style={[styles.slogan,{alignSelf:'center'}]}>{this.props.interesse}</Text></View>
        )
    }
}