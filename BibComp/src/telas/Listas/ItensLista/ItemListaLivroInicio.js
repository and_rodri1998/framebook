import React,{Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image
} from 'react-native';

import ElevatedView from 'react-native-elevated-view';
import Ripple from 'react-native-material-ripple';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import {styles} from '../../../style';
import {goSolLivro} from '../../../funcoes';
import {primaryLightColor} from '../../../config/constantes';

export default class ItemListaLivroInicio extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <ElevatedView elevation={2} style={{padding:5,alignItens:'center',marginBottom:10}}>
            	<View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
            		<View style={{alignItems:'center',flexDirection:'row'}}>
                        <View style={{alignItems:'center',justifyContent:'center',width:75, height: 75,borderRadius:10}}>
                            <Image
                              style={{width: 70, height: 70,borderRadius:10}}
                              source={this.props.capa}
                            />
                        </View>
                        <View style={{marginLeft:10}}>
                            <Text style={[styles.slogan,{justifyContent:'center',color:primaryLightColor}]}>{this.props.livro}</Text>
                            <Text style={[styles.slogan,{justifyContent:'center',color:primaryLightColor}]}>Autor: {this.props.autor}</Text>
                            <Text style={[styles.slogan,{justifyContent:'center',color:primaryLightColor}]}>Dono: {this.props.dono}</Text>
                        </View>
                    </View>
                    <View style={[styles.containerIconButton,{marginRight:10}]}>
                        <Ripple onPress={()=>goSolLivro()}>
                            <MaterialIcons name='add' size={40} color={primaryLightColor} />
                        </Ripple>
                        <Text style={{color: primaryLightColor,fontSize:12}}>Adicionar</Text>
                    </View>
            	</View>            	
            </ElevatedView>
        )
    }
}