import React, {Component} from 'react';
import { 
    SectionList
 } from "react-native";

 import ItemListaInteresse from './ItensLista/ItemListaInteresse';

 export default class ListaInteresse extends Component{

    constructor(props){
      super(props);
      this.state={
        interesses:null
      }
    }

    componentDidMount(){
        this.getPerfil();
    }

    getPerfil(){
        let uid=firebase.auth().currentUser.uid,user=null;
        firebase.database().ref('/users/'+uid).once('value').then((snapshot)=>{
            let {interesses}=snapshot.val();
            // alert(user.username);
            
            this.setState({interesses});
        })
    }
     render(){
         return (
            <SectionList
              renderItem={({ item, index, section }) => <ItemListaInteresse interesse={item}/>}
              sections={interesses}
              keyExtractor={(item, index) => index}
            />
         );
     }
 }