import React, {Component} from 'react';
import { 
    SectionList
 } from "react-native";

 import ItemListaLivro from './ItensLista/ItemListaLivro';

 export default class ListaLivros extends Component{
     render(){
        let itens=[
            { title: 'Title1', data: [{livro:'Livro 1',autor:'autor 1',dono:'dono 1',capa:require('../../assets/img/baseline_person_black_48.png'),id:1}] },
            { title: 'Title2', data: [{livro:'Livro 2',autor:'autor 2',dono:'dono 2',capa:require('../../assets/img/baseline_person_black_48.png'),id:2}] },
            { title: 'Title3', data: [{livro:'Livro 3',autor:'autor 3',dono:'dono 3',capa:require('../../assets/img/baseline_person_black_48.png'),id:3}] },
            { title: 'Title4', data: [{livro:'Livro 4',autor:'autor 4',dono:'dono 4',capa:require('../../assets/img/baseline_person_black_48.png'),id:4}] },
          ];
         return (
            <SectionList
              renderItem={({ item, index, section }) => <ItemListaLivro livro={item.livro} autor={item.autor} dono={item.dono} capa={item.capa}/>}
              sections={itens}
              keyExtractor={(item, index) => item.id}
            />
         );
     }
 }