import React,{Component} from 'react';
import {
    View,
    Text,
    ScrollView,
    TextInput,
    TouchableOpacity,
    StatusBar
} from 'react-native';

import firebase from 'react-native-firebase';

import {styles} from '../../style';
import {popSwitch,goInicio,logar} from '../../funcoes';
import {primaryLightColor} from '../../config/constantes';

export default class Login extends Component{
    constructor(props){
        super(props);
        this.state={
            email:'',
            senha:''
        }
    }

    componentDidMount(){
        firebase.auth().onAuthStateChanged(function(user) {
          if (user) {
            goInicio();
          }
        });
    }

    verificarCampos(){
        if(this.state.email==''||this.state.senha=='')
            alert('existem campos vazios');
        else{
            logar(this.state.email,this.state.senha);
        }
    }

    render(){
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={primaryLightColor}
                    barStyle="light-content"
                />
                <ScrollView contentContainerStyle={{flexGrow: 1}}>
                  <View style={styles.areaCenter}>
                    <Text style={styles.nameLog}>Frame Book</Text>
                    <TextInput
                        style={styles.inputLog}
                        onChangeText={(text) => {this.setState({email:text})}}
                        placeholder="Email"
                        keyboardType="email-address"
                        placeholderTextColor="#fff"
                    />
                    <TextInput
                        style={styles.inputLog}
                        onChangeText={(text) => {this.setState({senha:text})}}
                        placeholder="Senha"
                        placeholderTextColor="#fff"
                        secureTextEntry={true}
                    />
                    <View style={{width:'90%',flexDirection:'row',justifyContent:'space-between'}}>
                        <TouchableOpacity style={styles.btnLogCad} onPress={()=>popSwitch()}>
                            <Text style={styles.textBtnLog}>Voltar</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btnLogCad} onPress={()=>this.verificarCampos()}>
                            <Text style={styles.textBtnLog}>Entrar</Text>
                        </TouchableOpacity>
                    </View>
                  </View>
                </ScrollView>
            </View>
        )
    }
}
