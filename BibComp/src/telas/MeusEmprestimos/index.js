import React,{Component} from 'react';
import {
    View,
    Text,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Image,
    StatusBar,
    Picker
} from 'react-native';

import {CheckBox,SearchBar} from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
import Ripple from 'react-native-material-ripple';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ElevatedView from 'react-native-elevated-view';

import {styles} from '../../style';
import {goInicio,popToMeusEmpr,goDevolver} from '../../funcoes';
import {primaryTextColor,primaryLightColor,secondaryColor} from '../../config/constantes';
import HeaderComum from '../componentes/Header/HeaderComum';
import ListaInteresse from '../Listas/ListaInteresse';
import ListaLivrosInicio from '../Listas/ListaLivrosInicio';

export default class MeusEmprestimos extends Component{
    constructor(props){
        super(props);
        this.state={language:'1 semana',avatarSource:require('../../assets/img/baseline_person_black_48.png'),literatura:false,tecnologia:false,esportes:false,outros:false};
    }

    render(){

        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={primaryLightColor}
                    barStyle="light-content"
                />
                <HeaderComum title="Meus Empréstimos"/>
                <ScrollView contentContainerStyle={{flexGrow: 1}}>
                  <View style={[styles.areaConteudo,{paddingBottom:10}]}>
                    <View style={{marginTop:10,marginBottom:10, alignItems:'center'}}>
                        <View style={{padding:5,width: 105, height: 105,borderRadius:105,backgroundColor:'white',alignItems:'center', justifyContent:'center'}}>
                            <Image
                              style={{width: 100, height: 100,borderRadius:100}}
                              source={this.state.avatarSource}
                            />
                        </View>
                        <Text style={styles.slogan}>Nome</Text>
                        
                        <View style={{paddingHorizontal:10,width:'100%',marginTop:10}}>
                            <ElevatedView elevatio={2} style={{padding:10,borderRadius:10}}>
                                <Text style={[styles.slogan,{color:primaryLightColor,alignSelf:'center',marginBottom:10}]}>Meus Empréstimos</Text>
                                <View style={{flex:1,width:'90%',alignSelf:'center',marginBottom:10}}>
                                	<View style={{flexDirection:'row',width:'100%',justifyContent:'space-between'}}> 
                                		<Text style={[styles.slogan,{color:primaryLightColor}]}>Título</Text>
	                                	<Text style={[styles.slogan,{color:primaryLightColor}]}>Data</Text>
	                                	<Text style={[styles.slogan,{color:primaryLightColor}]}>Situação</Text>
                                	</View>
                                	<View style={{flexDirection:'row',width:'100%',justifyContent:'space-between'}}> 
                                		<TouchableOpacity onPress={()=>goDevolver()}>
                                            <Text style={[styles.slogan,{color:primaryLightColor}]}>Linha 1</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={()=>goDevolver()}>
                                            <Text style={[styles.slogan,{color:primaryLightColor}]}>linha 2</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={()=>goDevolver()}>
                                            <Text style={[styles.slogan,{color:primaryLightColor}]}>linha 3</Text>
                                        </TouchableOpacity>
                                	</View>
                                </View>

                               <TouchableOpacity style={[styles.btnLogCad,{width:'90%',alignSelf:'center',backgroundColor:primaryLightColor}]} onPress={()=>goInicio()}>
			                        <Text style={[styles.textBtnLog,{color:'#fff'}]}>Voltar</Text>
			                    </TouchableOpacity>
                            </ElevatedView>
                        </View>
                    </View>
                  </View>
                </ScrollView>
            </View>
        )
    }
}
