import React,{Component} from 'react';
import {
    View,
    Text,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Image,
    StatusBar
} from 'react-native';

import {CheckBox} from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';

import {styles} from '../../style';
import {goCadLivro,popToInicio} from '../../funcoes';
import {primaryTextColor,primaryLightColor} from '../../config/constantes';
import HeaderComum from '../componentes/Header/HeaderComum';
import ListaInteresse from '../Listas/ListaInteresse';
import ListaLivros from '../Listas/ListaLivros';

export default class MeusLivros extends Component{
    constructor(props){
        super(props);
        this.state={avatarSource:require('../../assets/img/baseline_person_black_48.png'),literatura:false,tecnologia:false,esportes:false,outros:false};
    }

    render(){

        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={primaryLightColor}
                    barStyle="light-content"
                />
                <HeaderComum title="Meus Livros"/>
                <ScrollView contentContainerStyle={{flexGrow: 1}}>
                  <View style={[styles.areaConteudo,{paddingBottom:10}]}>
                    <View style={{marginTop:10,marginBottom:10, alignItems:'center'}}>
                        <View style={{padding:5,width: 65, height: 65,borderRadius:65,backgroundColor:'white',alignItems:'center', justifyContent:'center'}}>
                            <Image
                              style={{width: 60, height: 60,borderRadius:60}}
                              source={this.state.avatarSource}
                            />
                        </View>
                        <Text style={styles.slogan}>Nome</Text>
                    </View>
                    <Text style={[styles.slogan,{marginBottom:10,marginLeft:10}]}>Meus livros</Text>
                    <View style={{paddingHorizontal:10}}>
                    	<ListaLivros />
                    </View>
                    <TouchableOpacity style={[styles.btnLogCad,{width:'90%',alignSelf:'center'}]} onPress={()=>goCadLivro()}>
                        <Text style={styles.textBtnLog}>Adicionar</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.btnLogCad,{width:'90%',alignSelf:'center',backgroundColor:primaryLightColor}]} onPress={()=>popToInicio()}>
                        <Text style={[styles.textBtnLog,{color:'#fff'}]}>Voltar</Text>
                    </TouchableOpacity>
                  </View>
                </ScrollView>
            </View>
        )
    }
}
