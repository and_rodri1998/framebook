import React,{Component} from 'react';
import {
    View,
    Text,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Image,
    StatusBar,
    ActivityIndicator
} from 'react-native';

import {CheckBox} from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
import firebase from 'react-native-firebase';

import {styles} from '../../style';
import {goCadLivro} from '../../funcoes';
import {primaryTextColor,primaryLightColor} from '../../config/constantes';
import HeaderComum from '../componentes/Header/HeaderComum';
import ListaInteresse from '../Listas/ListaInteresse';
import ListaLivros from '../Listas/ListaLivros';

export default class Perfil extends Component{
    constructor(props){
        super(props);
        this.state={user:''
            ,avatarSource:require('../../assets/img/baseline_person_black_48.png'),literatura:false,tecnologia:false,esportes:false,outros:false};
    }

    componentDidMount(){
        this.getPerfil();
    }

    getPerfil(){
        let uid=firebase.auth().currentUser.uid,user=null;
        firebase.database().ref('/users/'+uid).once('value').then((snapshot)=>{
            user=snapshot.val();
            // alert(user.username);
            
            this.setState({user});
        })
    }

    render(){

        let view=null;
        if(!this.state.user){

            view = <View style={styles.container}>
                        <StatusBar
                            backgroundColor={primaryLightColor}
                            barStyle="light-content"
                        />
                      <View style={styles.areaCenter}>
                        <ActivityIndicator size="large" />
                        <Text style={[styles.nameLog,{fontSize:16}]}>carregando</Text>
                        <Text style={[styles.nameLog,{fontSize:16}]}>dados</Text>
                      </View>
                    </View>
        }else{

            view =(<View style={styles.container}>
                <StatusBar
                    backgroundColor={primaryLightColor}
                    barStyle="light-content"
                />
                <HeaderComum title="Perfil"/>
                <ScrollView contentContainerStyle={{flexGrow: 1}}>
                  <View style={[styles.areaConteudo,{paddingBottom:10}]}>
                    <View style={{marginTop:10,marginBottom:10, alignItems:'center'}}>
                        <View style={{padding:5,width: 105, height: 105,borderRadius:105,backgroundColor:'white',alignItems:'center', justifyContent:'center'}}>
                            <Image
                              style={{width: 100, height: 100,borderRadius:100}}
                              source={{uri:this.state.user.imgperfil}}
                            />
                        </View>
                        <Text style={styles.slogan}>{this.state.user.endereco}</Text>
                        <Text style={styles.slogan}>{this.state.user.telefone}</Text>
                    </View>
                    <Text style={[styles.slogan,{alignSelf:'center',marginLeft:10}]}>Áreas de Interesse</Text>
                    
                    <Text style={[styles.slogan,{marginBottom:10,marginLeft:10}]}>Livros</Text>
                    <View style={{paddingHorizontal:10}}>
                    	<ListaLivros />
                    </View>
                    <TouchableOpacity style={[styles.btnLogCad,{width:'90%',alignSelf:'center'}]} onPress={()=>goCadLivro()}>
                        <Text style={styles.textBtnLog}>Adicionar</Text>
                    </TouchableOpacity>
                  </View>
                </ScrollView>
            </View>
            )
        }

        return view;
    }
}
