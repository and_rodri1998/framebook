import React,{Component} from 'react';
import {
    View,
    Text,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Image,
    StatusBar,
    Picker
} from 'react-native';

import {CheckBox,SearchBar} from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
import Ripple from 'react-native-material-ripple';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ElevatedView from 'react-native-elevated-view';

import {styles} from '../../style';
import {goInicio} from '../../funcoes';
import {primaryTextColor,primaryLightColor,secondaryColor} from '../../config/constantes';
import HeaderComum from '../componentes/Header/HeaderComum';
import ListaInteresse from '../Listas/ListaInteresse';
import ListaLivrosInicio from '../Listas/ListaLivrosInicio';

export default class SolicitacaoLivro extends Component{
    constructor(props){
        super(props);
        this.state={language:'1 semana',avatarSource:require('../../assets/img/baseline_person_black_48.png'),literatura:false,tecnologia:false,esportes:false,outros:false};
    }

    render(){

        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={primaryLightColor}
                    barStyle="light-content"
                />
                <HeaderComum title="Solicitar Livro"/>
                <ScrollView contentContainerStyle={{flexGrow: 1}}>
                  <View style={[styles.areaConteudo,{paddingBottom:10}]}>
                    <View style={{marginTop:10,marginBottom:10, alignItems:'center'}}>
                        <View style={{padding:5,width: 105, height: 105,borderRadius:105,backgroundColor:'white',alignItems:'center', justifyContent:'center'}}>
                            <Image
                              style={{width: 100, height: 100,borderRadius:100}}
                              source={this.state.avatarSource}
                            />
                        </View>
                        <Text style={styles.slogan}>Nome</Text>
                        
                        <View style={{paddingHorizontal:10,width:'100%',marginTop:10}}>
                            <ElevatedView elevatio={2} style={{padding:10,borderRadius:10}}>
                                <Text style={[styles.slogan,{color:primaryLightColor,alignSelf:'center',marginBottom:10}]}>Realizar Empréstimo</Text>
                                <View style={{alignSelf:'center',padding:5,width: 105, height: 105,borderRadius:10,backgroundColor:'white',alignItems:'center', justifyContent:'center'}}>
                                    <Image
                                      style={{width: 100, height: 100,borderRadius:100}}
                                      source={require('../../assets/img/baseline_book_black_48.png')}
                                    />
                                </View>
                                <Text style={[styles.slogan,{color:primaryLightColor,alignSelf:'center',marginBottom:10}]}>Nome do Livro</Text>
                                <Text style={[styles.slogan,{color:primaryLightColor,alignSelf:'center',marginBottom:10}]}>Edição</Text>
                                <View style={{flexDirection:'row', alignItems:'center',alignSelf:'center'}}>
                                    <Text style={[styles.slogan,{color:primaryLightColor,alignSelf:'center',marginBottom:10}]}>Devolução</Text>
                                    <Picker
                                        selectedValue={this.state.language}
                                        style={{ height: 50, width: 200 }}
                                        onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
                                      <Picker.Item label="1 semana" value="java" />
                                      <Picker.Item label="2 semanas" value="js" />
                                    </Picker>
                                </View>

                                <View style={{width:'100%',flexDirection:'row',justifyContent:'space-between'}}>
                                    <TouchableOpacity style={[styles.btnLogCad,{backgroundColor:secondaryColor}]} onPress={()=>goInicio()}>
                                        <Text style={[styles.textBtnLog,{color:'#fff'}]}>Voltar</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.btnLogCad,{backgroundColor:primaryLightColor}]} onPress={()=>goInicio()}>
                                        <Text style={[styles.textBtnLog,{color:'#fff'}]}>Finalizar</Text>
                                    </TouchableOpacity>
                                </View>
                            </ElevatedView>
                        </View>
                    </View>
                  </View>
                </ScrollView>
            </View>
        )
    }
}
