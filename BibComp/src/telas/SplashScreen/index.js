import React,{Component} from 'react';
import {
    View,
    Text,
    ScrollView,
    TextInput,
    TouchableOpacity,
    StatusBar,
    ActivityIndicator
} from 'react-native';

import firebase from 'react-native-firebase';

import {styles} from '../../style';
import {goSwitch,goInicio} from '../../funcoes';
import {primaryLightColor} from '../../config/constantes';

export default class SplashScreen extends Component{

    constructor(props){
        super(props);
    }

    componentDidMount(){
        firebase.auth().onAuthStateChanged((user) => {
            if(user){
                goInicio();
            }else{
                goSwitch();
            }
        });
    }

    render(){
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={primaryLightColor}
                    barStyle="light-content"
                />
              <View style={styles.areaCenter}>
                <ActivityIndicator size="large" />
                <Text style={styles.nameLog}>Aguarde ...</Text>
              </View>
            </View>
        )
    }
}
