import React,{Component} from 'react';
import {
    View,
    Text,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Image,
    StatusBar
} from 'react-native';

import {CheckBox,SearchBar} from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
import Ripple from 'react-native-material-ripple';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ElevatedView from 'react-native-elevated-view';

import {styles} from '../../style';
import {popToMeusEmpr,goMeusLivros} from '../../funcoes';
import {primaryTextColor,primaryLightColor} from '../../config/constantes';
import HeaderComum from '../componentes/Header/HeaderComum';
import ListaInteresse from '../Listas/ListaInteresse';
import ListaLivrosInicio from '../Listas/ListaLivrosInicio';

export default class TelaDevolver extends Component{
    constructor(props){
        super(props);
        this.state={avatarSource:require('../../assets/img/baseline_person_black_48.png'),literatura:false,tecnologia:false,esportes:false,outros:false};
    }

    render(){

        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={primaryLightColor}
                    barStyle="light-content"
                />
                <HeaderComum title="Devolução"/>
                <ScrollView contentContainerStyle={{flexGrow: 1}}>
                  <View style={[styles.areaConteudo,{paddingBottom:10}]}>
                    <View style={{marginTop:10,marginBottom:10, alignItems:'center'}}>
                        <View style={{padding:5,width: 105, height: 105,borderRadius:105,backgroundColor:'white',alignItems:'center', justifyContent:'center'}}>
                            <Image
                              style={{width: 100, height: 100,borderRadius:100}}
                              source={this.state.avatarSource}
                            />
                        </View>
                        <Text style={styles.slogan}>Nome</Text>
                        <View style={{paddingHorizontal:10,width:'100%',marginTop:10}}>
                            <ElevatedView elevatio={2} style={{padding:10,borderRadius:10}}>
                                <Text style={[styles.slogan,{color:primaryLightColor,alignSelf:'center',marginBottom:10}]}>Nome livro</Text>
                                <Text style={[styles.slogan,{color:primaryLightColor,alignSelf:'center',marginBottom:10}]}>Empréstimo: 01/10/2018</Text>
                                <Text style={[styles.slogan,{color:primaryLightColor,alignSelf:'center',marginBottom:10}]}>Devolução: 10/10/2018</Text>
                                <Text style={[styles.slogan,{color:primaryLightColor,alignSelf:'center',marginBottom:10}]}>Dono: Nome</Text>
                                <TouchableOpacity style={[styles.btnLogCad,{width:'90%',alignSelf:'center',backgroundColor:primaryLightColor}]} onPress={()=>popToMeusEmpr()}>
                                    <Text style={[styles.textBtnLog,{color:'#fff'}]}>Devolver</Text>
                                </TouchableOpacity>
                            </ElevatedView>
                        </View>
                    </View>
                  </View>
                </ScrollView>
            </View>
        )
    }
}
