import React,{Component} from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    StatusBar
} from 'react-native';

import {styles} from '../../style';
import { goLogin, goCadastro } from '../../funcoes';
import {primaryLightColor} from '../../config/constantes';

export default class TelaSwitch extends Component{
    render(){
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={primaryLightColor}
                    barStyle="light-content"
                />
                <View style={styles.areaConteudoSwitch}>
                    <View style={{alignItems:'center',marginTop:20}}>
                        <Text style={styles.nameLog}>Frame Book</Text>
                        <Text style={styles.slogan}>
                            "...Todo um mundo de fantasias,
                        </Text>
                        <Text style={styles.slogan}>
                            espera por você..."
                        </Text>
                    </View>
                    <View style={{alignItems:'center',marginBottom:40}}>
                        <TouchableOpacity style={styles.buttonSwitch} onPress={()=>goLogin()}>
                            <Text style={styles.textBtnSwitch}>Entrar</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonSwitch} onPress={()=>goCadastro()}>
                            <Text style={styles.textBtnSwitch}>Cadastrar-se</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}
