import React,{Component} from 'react';
import {
	View,
	Text
} from 'react-native';

import {styles} from '../../../../style';
import {primaryLightColor} from '../../../../config/constantes';

export default class HeaderComum extends Component{
	constructor(props){
		super(props);
	}
	render(){
		return (
			<View style={styles.header}>
				<Text style={[styles.slogan,{fontSize:22}]}>{this.props.title}</Text>
			</View>
		);
	}
}