import React,{Component} from 'react';
import {
	View,
	Text
} from 'react-native';

import Ripple from 'react-native-material-ripple';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import {logout} from '../../../../funcoes';
import {styles} from '../../../../style';
import {primaryLightColor} from '../../../../config/constantes';

export default class HeaderInicio extends Component{
	constructor(props){
		super(props);
	}
	render(){
		return (
			<View style={styles.header}>
				<Text style={[styles.slogan,{fontSize:22}]}>{this.props.title}</Text>
				<View style={styles.containerIconButton}>
                    <Ripple onPress={()=>logout()}>
                        <MaterialIcons name='exit-to-app' size={28} color='#fff' />
                    </Ripple>
                </View>
			</View>
		);
	}
}